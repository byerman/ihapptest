package com.huaweisoft.helmettest;

import android.app.Application;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //支持读取本地配置
        EmasInit emas = EmasInit.getInstance();
        //初始化高可用
        emas.initHA();
        //初始化应用更新
        emas.initUpdate();
        //初始化Weex
        emas.initWeex();
    }
}
